# Linux Command List 

> Commands marked wth ** will be discussed later

## Day 1
- NA

## Day 2
1. echo
1. date
1. pwd
1. clear
1. sleep
1. env
1. man
1. time
1. mkdir **
1. cat **


## Day 3
1. cal
1. history
1. seq
1. history
1. passwd
1. exit
1. touch **
1. ls **

## Day 4 & 5
1. mkdir
1. touch
1. ln
1. ls
1. tree
1. cd
1. rmdir
1. cp  ** day 5
1. mv
1. rm
1. unlink
1. du
1. df
