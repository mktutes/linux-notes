text processing

	pattern language: regex
	stream oriented: grep, sed, awk
	interactive: vim

grep, sed, awk

- use similar syntax
- stream-oriented, read one rec at a time, process and write to stdout
- use regex for matching patterns
- instructions can be written in a file as script and passed to command
- grep: filter
- sed: replace
- awk: sed+grep, transform


grep

- grep: g/re/p - global regular expression and print
- named derived from text filter syntax of the 'ed' editor of old times
- match regex and filter records 


- regex should be quoted
- [] is a charset in both regex and wildcard
- a command is processed by shell first
- quoting regex will avoid situations where metachars and wildcards are recogized properly
- example:
	$ grep [A-Z]* sample_0*.txt
	# expanded as
	grep Addr.txt System.cfg sample_01.txt sample_02.txt

	# single quote is safest, may use double qoute as well
	$ grep "[A-Z]*"" sample_0*.txt
	# expanded as
	grep "[A-Z]*" sample_01.txt sample_02.txt


----

sed

- non-interactive, stream editor
- input from file or redirected from a command
- filter and modify used
- popular use case: substitute or search and replace option
- input is left as-it-is by default

sed syntax
   sed [options] [commands] [files]
   
   sed [-n] [-e] 'command' file(s)
   sed [-n] -f script file(s)

- each line is copied to a "pattern space" and editing is performed
- pattern space == workspace or buffer
- edit via sed commands in the order they occur in the command
- each command will use the modified buffer if changed before execution
- line from buffer is printed on screen or redirected to file / cmd
- has a "hold space" like buffer for later data retrieval

options
	-n : suppress print
	-e : expression, a command will follow
	-f : read commands from a file
	-i : in-line edit

commands
	s : substitute
	d : delete
	p : print
	q : quit
	y : translate - like tr command

	a\ : append text after line
	i\ : insert text before line
	c\ : replace text (usually a block of text)

	= : display line #
	l : display control chars in ascii

sed command syntax
	[address[, address]][!]command [arguments]

	address: line number, symbol ($), /regex/ pattern (optional)
		if no address, command is applied to every line
		can specify 0, 1 or 2 addresses
		some commands accept only one addr: a, i, r, q, =
		2 addr: from first matching line to the last line matching 2nd addr (inclusive)

	! : all lines that did not match the address 

	- all edit commands are applied to each line, if no addr specified
	- commands applied in the order they appear to each selected lines
	- original is unchanged by default
	- each command will pickup the modified data from previous command
		s/a/b/g s/b/c/g => changes all a and b to c in each line


	examples:
		's/something//' : remove something from input 
		's/srch/repl/'  : replace first occurence only
		's/srch/repl/g' : replace all occurences
		's/srch/repl/n' : replace nth occurence
		`s/a/b/g;s/c/d/2' : run multiple commands
		/^#/,/end/!s/def/func/g : replace all occrences of def with func except lines that start with a comment (#) or line that has end in it

		'/unix/d' : delete all lines that has 'unix'
		'/^def'/,\end\s*$/p' : print all lines from def to end (ruby)
		'/^#/!d' : delete all lines that does not start with '#'

	braces can be used to apply different commands to same address
		opening brace at end of line
		closing brace by itself
		no spaces before braces
		[addr, [addr]]{
			cmd1
			cmd2
			cmd3
		}

Sample file
$ cat toppings.txt
Pizza topping combos:
1. Spinach, Pepperoni, Pineapple
2. Pepporoni, Pineapple, Mushroom
3. Bacon, Banana, Peppers, Pineapple
4. Cheese, Pineapple

$ sed 's/Pineapple/Olives/' toppings.txt
# pineapple changed to olives
# output displayed on screen, original file untouched
# we can redirect the output, rename to update the changes

$ sed -i .... => in-place updates, original file overwritten
  -i.bak => create orig_file.bak before upating
  -e 's///' => multiple -e to code one expression per option

 s/// : / is the delimiter
 we can use space, dot, pipe or any punctuation

 `s!!!`, `s|||` 's...' 's xyz abc ' still work the same, '/' is used commonly but we can use others if the data itself has a '/'

# input has '/' so we use 's!!!' instead of 's///'
$ echo "/etc/passed" | sed 's!/etc/!!'
passwd

----
s: substitute
	[addr, [addr]]!s/srch/repl/[g<N>]

	s/srch/repl/ 	: replace first occurence from all matched lines
	s/srch/repl/g 	: replace all occurence from all matched lines
	s/srch/repl/N 	: replace Nth occurence from all matched lines
	s/srch// 		: remove first occurence from all matched lines

	`s/S/R/; s/S/R/` : multiple commands
	-e 's/S/R/' -e 's/S/R/' : same as above

```
$ echo "aa bb aa bb aa aa bb" | sed 's/aa/AA/'
AA bb aa bb aa aa bb
$ echo "aa bb aa bb aa aa bb" | sed 's/aa/AA/g'
AA bb AA bb AA AA bb
$ echo "aa bb aa bb aa aa bb" | sed 's/aa/AA/2'
aa bb AA bb aa aa bb
$ echo "aa bb aa bb aa aa bb" | sed 's/aa/AA/3'
aa bb aa bb AA aa bb
$ echo "aa bb aa bb aa aa bb" | sed 's/aa/AA/4'
aa bb aa bb aa AA bb
```

```
$ echo "aa bb aa bb aa aa bb" | sed -e 's/aa //'
bb aa bb aa aa bb
$ echo "aa bb aa bb aa aa bb" | sed -e 's/aa //2'
aa bb bb aa aa bb
$ echo "aa bb aa bb aa aa bb" | sed -e 's/aa //3'
aa bb aa bb aa bb
$ echo "aa bb aa bb aa aa bb" | sed -e 's/aa //4'
aa bb aa bb aa bb
$ echo "aa bb aa bb aa aa bb" | sed -e 's/aa //5'
aa bb aa bb aa aa bb
$ echo "aa bb aa bb aa aa bb" | sed -e 's/aa //g'
bb bb bb
```

```
$ echo "aa bb aa bb aa aa bb" | sed 's/aa/AA/4; s/bb/BB/2'
aa bb aa BB aa AA bb
$ echo "aa bb aa bb aa aa bb" | sed -e 's/aa/AA/4' -e 's/bb/BB/2'
aa bb aa BB aa AA bb
```

d: delete

	d       : delete all lines
	1d      : delete first line
	$d      : delete last line
	
	10,20d  : delete lines 10-20
	10,$d   : delete all lines from line number 10

	10,+10d : delete 10th record and 10 records after that
	1~2d    : delete all odd numbered records
	0~2d    : delete all even numbered records

	/^$/d   : delete all empty lines
	/^\s*$/d: delete all blank or empty lines

	/^START/,/^END/d : delete all lines between START and END; inclusive

	1,/^$/d    : delete from the first line upto a blank line, delete the blank line as well

	1,5!d  : delete everything other than lines 1-5

	sed '/from/,/while/{/^ *$/d}' address.py
		delete all the blank lines between line with 'from' and line with 'while'
		
