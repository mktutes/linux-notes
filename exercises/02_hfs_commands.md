# Linux: File System Commands (HFS)

> Make use of the `man` command to learn more about the functionality and the options

> Try using `man` before searching online  

## Command Set

> Review the above commands and understand the functionality and options 

||||||
|---|----|----|----|----|
|mkdir|touch|ln|cd|rmdir|
|cp|mv|rm|unlink|file|
|ls|tree|dir|du|df|

## Problem Set 1
1. create the following directories `projA`, 'projB`, 'projC`
1. create the following directory called `projD` and make the command display the outcome. 
1. what happens when we try to create a same directory twice
1. what happens when we try to create a directory whose parent is not present; `projA/data/input` for example
1. how can we create the missing parent or ancester directories while creating a directory
1. what does `cd` without any argument would do
1. are there any diffrences between the way `cd` and `cd ~` work
1. how do we toggle between previous working directory and current working directory ?
1. how to delete a directory that is empty
1. how to delete a directory that is not empty


## Problem Set 2
1. which command is used to create a file called `sample.txt`
1. what happens if we try to create a file that exists already
1. what are the few advantages of using the command to create empty file(s)
1. create a file called `student_data_file_YYYYMMDD.csv` and create a shortcut `student.csv`


## Problem Set 

1. create the following directories by executing just one command
```
processor
├── amd
│   ├── bin
│   ├── data
│   ├── etc
│   ├── lib
│   └── log
└── intel
    ├── bin
    ├── data
    ├── etc
    ├── lib
    └── log
```

1. once the directories are created as above, display the directories in the same format shown above

1. how to accomplish the following change (date/time of file)

```
$ CMD sample.txt
$ ls -l sample.txt
-rw-r--r-- 1 mktutes mktutes 0 Aug 29 06:36 sample.txt

$ CMO OPTION(S) sample.txt
$ ls -l sample.txt
-rw-r--r-- 1 mktutes mktutes 0 Jan 29  2021 sample.txt

$ CMO OPTION(S) sample.txt
$ ls -l sample.txt
-rw-r--r-- 1 mktutes mktutes 0 Nov 29  2021 sample.txt
```

