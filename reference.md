# Linux: Useful Links
**Articles, Videos, Books, etc...**


## Free Ebooks
- [The Linux Command Line by Willian Shotts](https://linuxcommand.org/tlcl.php)
- [AWK Programming Language by the Creators](https://ia903404.us.archive.org/0/items/pdfy-MgN0H1joIoDVoIC7/The_AWK_Programming_Language.pdf)

## Articles and Blogs
- [sed tutorial and reference](https://alexharv074.github.io/2019/04/16/a-sed-tutorial-and-reference.html#sed-command-cheat-sheet)
by **Alex Harvey @Github.io**

## Videos
- [CLI from MIT Missing Lectures 2020](https://missing.csail.mit.edu/2020/command-line/)

## Vi Editor
- [Byte of Vim by Swaroop](https://vim.swaroopch.com/byte-of-vim.pdf) **Book**
- [Missing Lectures @ MIT](https://missing.csail.mit.edu/2020/editors/) **Video**


## Shell Scripting
- [Bash Arrays](https://www.freecodecamp.org/news/bash-array-how-to-declare-an-array-of-strings-in-a-bash-script/)